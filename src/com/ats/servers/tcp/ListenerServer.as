package com.ats.servers.tcp {
    import com.ats.managers.RunningDevicesManager;

    import flash.events.Event;
    import flash.events.IOErrorEvent;
    import flash.events.ProgressEvent;
    import flash.events.ServerSocketConnectEvent;
    import flash.filesystem.File;
    import flash.net.ServerSocket;
    import flash.net.Socket;

    import mx.core.FlexGlobals;

    public class ListenerServer {

        public function ListenerServer() {
            try {
                serverSocket = new ServerSocket();
                serverSocket.addEventListener(Event.CONNECT, connectHandler);
                serverSocket.addEventListener(Event.CLOSE, onClose);
                serverSocket.bind(0, "127.0.0.1")
            } catch (e:SecurityError) {
                trace(e);
            }
        }
        private var serverSocket:ServerSocket;

        public function start():int {
            serverSocket.listen()
            return serverSocket.localPort
        }

        public function stop():void {
            serverSocket.close()
        }

        private var currentSocket:Socket

        private function connectHandler(event:ServerSocketConnectEvent):void {
            var socket:Socket = event.socket
            socket.addEventListener(ProgressEvent.SOCKET_DATA, socketDataHandler)
            socket.addEventListener(Event.CLOSE, onClientClose)
            socket.addEventListener(IOErrorEvent.IO_ERROR, onIOError)
            currentSocket = socket
        }

        private var devices:RunningDevicesManager = RunningDevicesManager(FlexGlobals.topLevelApplication.devices)

        private function socketDataHandler(event:ProgressEvent):void {
            var socket:Socket = Socket(event.target)

            var filePath:String = socket.readUTFBytes(socket.bytesAvailable)
            var file:File = new File(filePath)
            if (file.exists) {
                devices.addEventListener(RunningDevicesManager.INSTALL_COMPLETE, onInstallComplete)
                devices.installAppFile(file)
            }
        }

        private function onInstallComplete(event:Event):void {
            currentSocket.writeUTFBytes("Install complete")
            currentSocket.flush()
        }

        private function onClientClose(event:Event):void {
            var socket:Socket = Socket(event.target)
            socket.removeEventListener(ProgressEvent.SOCKET_DATA, socketDataHandler)
            socket.removeEventListener(Event.CLOSE, onClientClose)
            socket.removeEventListener(IOErrorEvent.IO_ERROR, onIOError)

            currentSocket = null
            devices.removeEventListener(RunningDevicesManager.INSTALL_COMPLETE, onInstallComplete)
        }

        private static function onIOError(errorEvent:IOErrorEvent):void {
            trace("IOError: " + errorEvent.text)
        }

        private static function onClose(event:Event):void {
            trace("Server socket closed by OS.")
        }
    }
}
