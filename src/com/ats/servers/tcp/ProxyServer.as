package com.ats.servers.tcp {
    import com.ats.device.running.android.RunningServerSocket;
    import com.ats.helpers.PortSwitcher;
    import com.worlize.websocket.WebSocket;
    import com.worlize.websocket.WebSocketConfig;
    import com.worlize.websocket.WebSocketErrorEvent;
    import com.worlize.websocket.WebSocketEvent;

    import flash.events.Event;
    import flash.events.EventDispatcher;
    import flash.events.OutputProgressEvent;
    import flash.events.ProgressEvent;
    import flash.events.ServerSocketConnectEvent;
    import flash.net.Socket;
    import flash.utils.ByteArray;

    public class ProxyServer extends EventDispatcher {
        private static var runningServerSocket:Vector.<RunningServerSocket> = new Vector.<RunningServerSocket>();

        public static const WEB_SERVER_INITIALIZED:String = "webServerInitialized";
        public static const WEB_SERVER_STARTED:String = "webServerStarted";
        public static const WEB_SERVER_ERROR:String = "webServerError";

        public static const ALL_SERVERS_CLOSED:String = "allServersClosed";

        private var webSocket:WebSocket
        private var count:int = 0

        private var proxySockets:Vector.<ProxySocket> = new Vector.<ProxySocket>();

        public var running:Boolean = true;

        public var localPort:int = 0;

        public function close():void {
            running = false;

            while (proxySockets != null && proxySockets.length > 0) {
                var ps:ProxySocket = proxySockets.removeAt(0) as ProxySocket;

                ps.socket.removeEventListener(ProgressEvent.SOCKET_DATA, onClientSocketData);
                ps.socket.removeEventListener(OutputProgressEvent.OUTPUT_PROGRESS, onClientSocketOutputProgress);

                ps.dispose();
            }

            proxySockets = null;

            if (webSocket != null) {
                webSocket.removeEventListener(WebSocketEvent.MESSAGE, webSocketOnMessageHandler);
                webSocket.removeEventListener(WebSocketErrorEvent.CONNECTION_FAIL, webSocketConnectionFailHandler);
                webSocket.close(true);

                webSocket = null;
            }

            for each(var servSock:RunningServerSocket in runningServerSocket) {
                if (servSock.localPort == localPort) {
                    servSock.removeListener(onConnect);
                    break;
                }
            }
        }

        public function ProxyServer():void {
        }

        public function bind(port:int, autoPort:Boolean):void {
            var servSock:RunningServerSocket;
            for each(servSock in runningServerSocket) {
                if (servSock.localPort == port && !servSock.used) {

                    servSock.listener = onConnect;

                    localPort = port;
                    dispatchEvent(new Event(ProxyServer.WEB_SERVER_INITIALIZED));

                    return;
                }
            }

            if (autoPort) {
                port = PortSwitcher.generateAvailablePort(port);
            } else {
                if (!PortSwitcher.checkSocketPortAvailability(port)) {
                    trace("Web server bind port (" + port + ") error : Port already used !")
                    dispatchEvent(new Event(ProxyServer.WEB_SERVER_ERROR));
                    return;
                }
            }

            try {
                runningServerSocket.push(new RunningServerSocket(port, onConnect));
                localPort = port;

                dispatchEvent(new Event(ProxyServer.WEB_SERVER_INITIALIZED));

            } catch (e:Error) {
                trace("Web server bind port (" + port + ") error : " + e.message)
                dispatchEvent(new Event(ProxyServer.WEB_SERVER_ERROR));
            }
        }

        public function setupWebSocket(port:int):void {
            webSocket = new WebSocket("ws://localhost:" + port, "*");

            var webSocketConfig:WebSocketConfig = new WebSocketConfig();
            webSocketConfig.maxReceivedFrameSize = 0x600000;
            webSocket.config = webSocketConfig;

            webSocket.addEventListener(WebSocketEvent.OPEN, webSocketOpenHandler, false, 0, true);
            webSocket.addEventListener(WebSocketEvent.MESSAGE, webSocketOnMessageHandler, false, 0, true);
            webSocket.addEventListener(WebSocketErrorEvent.CONNECTION_FAIL, webSocketConnectionFailHandler, false, 0, true);

            dispatchEvent(new Event(ProxyServer.WEB_SERVER_STARTED));
        }

        private function webSocketOnMessageHandler(event:WebSocketEvent):void {
            var buffer:ByteArray = event.message.binaryData;

            var socketID:int = buffer.readInt();
            var socket:Socket = fetchSocket(socketID);
            if (socket != null) {
                socket.writeBytes(buffer, 4, buffer.length - 4);
                socket.flush();
            }
        }

        private static function webSocketConnectionFailHandler(event:WebSocketErrorEvent):void {
            trace("WebSocket ERROR : " + event.text);
        }

        private function webSocketOpenHandler(event:WebSocketEvent):void {
            webSocket.removeEventListener(WebSocketEvent.OPEN, webSocketOpenHandler);

            for each(var proxySocket:ProxySocket in proxySockets) {
                webSocket.sendBytes(proxySocket.data);
            }
        }

        private function onConnect(event:ServerSocketConnectEvent):void {
            // create new
            var proxySocket:ProxySocket = new ProxySocket(event.socket);

            proxySocket.socket.addEventListener(ProgressEvent.SOCKET_DATA, onClientSocketData, false, 0, true);
            proxySocket.socket.addEventListener(OutputProgressEvent.OUTPUT_PROGRESS, onClientSocketOutputProgress, false, 0, true);

            proxySocket.id = count;
            count++;

            proxySockets.push(proxySocket);
        }

        // 1. get from client
        private function onClientSocketData(event:ProgressEvent):void {
            var socket:Socket = event.target as Socket;
            var proxySocket:ProxySocket = fetchProxySocket(socket);
            // read data

            var clientData:ByteArray = new ByteArray();
            var tempData:ByteArray = new ByteArray();

            // websocket source
            // clientData.writeInt(0)

            // socket id
            clientData.writeInt(proxySocket.id);

            socket.readBytes(tempData, 0, socket.bytesAvailable);
            clientData.writeBytes(tempData, 0, tempData.length);

            proxySocket.data = clientData;

            // forward data
            if (webSocket.connected) {
                webSocket.sendBytes(clientData);
            } else {
                webSocket.connect();
            }
        }

        private function onClientSocketOutputProgress(event:OutputProgressEvent):void {
            if (event.bytesPending == 0) {
                var socket:Socket = event.target as Socket;
                socket.close();

                removeProxySocket(socket);
            }
        }

        private function onSocketClose(ev:Event):void {
            var socket:Socket = ev.target as Socket;
            removeProxySocket(socket);
        }

        /////

        private function fetchProxySocket(socket:Socket):ProxySocket {
            for each (var proxySocket:ProxySocket in proxySockets) {
                if (proxySocket.socket === socket) {
                    return proxySocket;
                }
            }

            return null;
        }

        private function fetchSocket(socketID:int):Socket {
            for each (var proxySocket:ProxySocket in proxySockets) {
                if (proxySocket.id == socketID) {
                    return proxySocket.socket;
                }
            }

            return null;
        }

        private function removeProxySocket(socket:Socket):void {
            socket.removeEventListener(ProgressEvent.SOCKET_DATA, onClientSocketData);
            socket.removeEventListener(OutputProgressEvent.OUTPUT_PROGRESS, onClientSocketOutputProgress);
            socket.removeEventListener(Event.CLOSE, onSocketClose);

            var proxySocket:ProxySocket = fetchProxySocket(socket);
            if (proxySocket != null) {
                var index:int = proxySockets.indexOf(proxySocket);
                proxySockets.removeAt(index);
            }
        }
    }
}