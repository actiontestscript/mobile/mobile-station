package com.ats.servers.tcp {
	import flash.net.Socket;
	import flash.utils.ByteArray;
	
	public class ProxySocket {
		
		public var socket:Socket;
		public var id:int;
		public var data:ByteArray;
		
		public function ProxySocket(socket:Socket) {
			this.socket = socket;
		}
		
		public function dispose():void{
			if(socket != null){
				socket.close();
			}
			socket = null;
		}
	}
}
