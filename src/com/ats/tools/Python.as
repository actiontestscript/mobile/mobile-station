package com.ats.tools
{
import com.ats.helpers.Settings;
import com.ats.managers.gmsaas.GmsaasInstaller;
import com.ats.servers.tcp.ListenerServer;
import com.greensock.TweenMax;

import flash.desktop.NativeApplication;

import flash.desktop.NativeProcess;
import flash.desktop.NativeProcessStartupInfo;
import flash.events.Event;
import flash.events.EventDispatcher;
import flash.events.IOErrorEvent;
import flash.events.NativeProcessExitEvent;
import flash.events.ProgressEvent;
import flash.filesystem.File;
import flash.net.URLLoader;
import flash.net.URLRequest;

public class Python extends EventDispatcher
	{
		public static const HTTP_SERVER_STARTING:String = "starting"
		public static const HTTP_SERVER_UP:String = "up"
		public static const HTTP_SERVER_DOWN:String = "down";

		[Bindable]
		public static var httpServerStatus:String

		public static const pythonFolderPath:String = "assets/tools/python";

		public static const workFolder:File = File.userDirectory.resolvePath("AppData/Roaming/Python/Python38")
		private static const scriptsFolder:File = File.applicationDirectory.resolvePath("assets/scripts")

		private static const updateScript:String = "update_app.py";
		private static const updateScriptMac:String = "update-macos-app.py";
		private static const getPipScript:String = "get-pip.py";
		private static const webServerScript:String = "ms_server.py";

		[Bindable]
		public static var file:File;

		[Bindable]
		public static var folder:File;

		public static var path:String;

		public function Python() {
			if (!Settings.isMacOs) {
				var assetsPythonFile:File = File.applicationDirectory.resolvePath(pythonFolderPath);
				if (assetsPythonFile.exists) {
					file = workFolder.resolvePath("python.exe");

					if (!workFolder.exists || !file.exists) {
						assetsPythonFile.copyTo(workFolder);
					}

					folder = file.parent;
					path = folder.nativePath;
				}
			}
		}

		public function install():void {
			if (folder.resolvePath("Scripts").resolvePath(GmsaasInstaller.pipFileName).exists) {
				dispatchEvent(new Event(Event.COMPLETE));
				return
			}

			var procInfo:NativeProcessStartupInfo = new NativeProcessStartupInfo();
			procInfo.executable = file;
			procInfo.workingDirectory = file.parent
			procInfo.arguments = new <String>[scriptsFolder.resolvePath(getPipScript).nativePath, "--no-warn-script-location"]

			var proc:NativeProcess = new NativeProcess();
			proc.addEventListener(NativeProcessExitEvent.EXIT, pipInstallExitHandler);
			proc.start(procInfo);
		}

		private function pipInstallExitHandler(ev:NativeProcessExitEvent):void{
			dispatchEvent(new Event(Event.COMPLETE));
		}

		public function updateApp(zipFile:File, appName:String):void {
			var info:NativeProcessStartupInfo = new NativeProcessStartupInfo();
			info.executable = file;
			info.workingDirectory = file.parent

			var path:String = File.applicationDirectory.nativePath;
			var parent:File = new File(path);
			parent = parent.parent;

			var script:File
			var arguments:Vector.<String>
			if (Settings.isMacOs) {
				script = scriptsFolder.resolvePath(updateScriptMac)
				appName += ".app"
				arguments = new <String>[
					StringHelper.unescapeFilePath(script),
					StringHelper.unescapeFilePath(zipFile),
					StringHelper.unescapeFilePath(parent.parent.parent),
					appName]
			} else {
				script = scriptsFolder.resolvePath(updateScript)

				arguments = new <String>[
					StringHelper.unescapeFilePath(script),
					StringHelper.unescapeFilePath(zipFile),
					StringHelper.unescapeFilePath(File.applicationDirectory),
					StringHelper.unescapeFilePath(File.applicationDirectory.resolvePath("AtsMobileStation.exe"))]
			}

			info.arguments = arguments

			var process:NativeProcess = new NativeProcess();
			process.start(info);

			NativeApplication.nativeApplication.exit();
		}

		// ---------

		private var outputData:String
		private var errorData:String

		public function setupMacOsPath():void {
			var startupInfo:NativeProcessStartupInfo = new NativeProcessStartupInfo()
			startupInfo.executable = new File("/usr/bin/env")
			startupInfo.arguments = new <String>["which", "python3"]

			outputData = ""
			errorData = ""

			var process:NativeProcess = new NativeProcess()
			process.addEventListener(ProgressEvent.STANDARD_OUTPUT_DATA, process_standardOutputDataHandler)
			process.addEventListener(ProgressEvent.STANDARD_ERROR_DATA, process_standardErrorDataHandler)
			process.addEventListener(NativeProcessExitEvent.EXIT, process_exitHandler)
			process.start(startupInfo)
		}

		private function process_standardOutputDataHandler(event:ProgressEvent):void {
			var process:NativeProcess = event.currentTarget as NativeProcess
			outputData += process.standardOutput.readUTFBytes(process.standardOutput.bytesAvailable)
		}

		private function process_standardErrorDataHandler(event:ProgressEvent):void {
			var process:NativeProcess = event.currentTarget as NativeProcess
			errorData += process.standardError.readUTFBytes(process.standardError.bytesAvailable)
		}

		private function process_exitHandler(event:NativeProcessExitEvent):void {
			var process:NativeProcess = event.currentTarget as NativeProcess
			process.removeEventListener(ProgressEvent.STANDARD_OUTPUT_DATA, process_standardOutputDataHandler)
			process.removeEventListener(ProgressEvent.STANDARD_ERROR_DATA, process_standardErrorDataHandler)
			process.removeEventListener(NativeProcessExitEvent.EXIT, process_exitHandler)

			if (errorData) {
				trace(errorData)
			}

			if (outputData) {
				file = new File(outputData.replace("\n", ""))
				folder = file.parent
				path = folder.nativePath
			}

			outputData = null
			errorData = null

			dispatchEvent(new Event(Event.COMPLETE))
		}


		/////////////////
		// HTTP Server //
		/////////////////

		private static var starting:Boolean = true;
		public function prepareHttpServer():void {
			stopHttpServer();
		}
		
		public function startHttpServer(port:String):void {
			copyHtmlFolder()
			launchHttpServer(port)
		}
		
		public function stopHttpServer():void {
			var urlLoader:URLLoader = new URLLoader()
			urlLoader.addEventListener(Event.COMPLETE, loaderCompleteHandler)
			urlLoader.addEventListener(IOErrorEvent.IO_ERROR, loaderCompleteHandler)

			var request:URLRequest = new URLRequest("http://localhost:" + currentServerPort + "/quit")
			urlLoader.load(request)
		}

		private function loaderCompleteHandler(event:Event):void {
			URLLoader(event.target).removeEventListener(Event.COMPLETE, loaderCompleteHandler)
			URLLoader(event.target).removeEventListener(IOErrorEvent.IO_ERROR, loaderCompleteHandler)

			currentServerPort = 0;
			httpServerStatus = HTTP_SERVER_DOWN;
			
			if(starting){
				starting = false;
				startHttpServer(Settings.getInstance().webServerPort.toString());
			}
		}

		private static function copyHtmlFolder():void {
			var htmlFolder:File = File.applicationDirectory.resolvePath("assets/http")
			htmlFolder.copyTo(File.userDirectory.resolvePath(".atsmobilestation/http"), true)
		}

		private static var httpServerErrorData:String
		private var listenerServer:ListenerServer = new ListenerServer()
		private var process:NativeProcess;
		private var info:NativeProcessStartupInfo;
			
		private function launchHttpServer(port:String):void {
			currentServerPort = parseInt(port);
			
			httpServerErrorData = ""
			httpServerStatus = HTTP_SERVER_STARTING

			var listenPort:int = listenerServer.start()

			var arguments: Vector.<String> =  new <String>[
				StringHelper.unescapeFilePath(scriptsFolder.resolvePath(webServerScript)),
				port,
				StringHelper.unescapeFilePath(Settings.adbFile),
				StringHelper.unescapeFilePath(Settings.httpFolder),
				listenPort.toString()
			]

			if (Settings.isMacOs) {
				var mobileDeviceFile:File = File.applicationDirectory.resolvePath("assets/tools/ios").resolvePath("mobiledevice")
				arguments.push(StringHelper.unescapeFilePath(mobileDeviceFile))
			}

			info = new NativeProcessStartupInfo();
			info.executable = file;
			info.workingDirectory = Settings.httpFolder
			info.arguments = arguments

			process = new NativeProcess()
			process.addEventListener(NativeProcessExitEvent.EXIT, onStartHttpServerExit)
			process.start(info)

			var urlLoader:URLLoader = new URLLoader()
			urlLoader.addEventListener(Event.COMPLETE, statusCompleteHandler)
			urlLoader.addEventListener(IOErrorEvent.IO_ERROR, statusCompleteHandler)

			var request:URLRequest = new URLRequest("http://localhost:" + port + "/status")
			TweenMax.delayedCall(2, urlLoader.load, [request])
		}
		
		private function statusCompleteHandler(event:Event):void {
			var urlLoader:URLLoader = event.currentTarget as URLLoader;
			if(urlLoader.data == "MS_SERVER_STARTED"){
				httpServerStatus = HTTP_SERVER_UP
			}else{
				httpServerStatus = HTTP_SERVER_DOWN;
				currentServerPort = -1;
				if(process != null && process.running){
					process.exit(true);
				}
			}
		}

		// to activate in dev mode !
		/*private function onStartHttpServerErrorData(event:ProgressEvent):void {
			var process:NativeProcess = NativeProcess(event.target)
			var errorData:String = process.standardError.readUTFBytes(process.standardError.bytesAvailable)
			httpServerErrorData += errorData
			trace(errorData)
		}*/

		[Bindable]
		public static var currentServerPort:int = Settings.getInstance().webServerPort;
		
		private function onStartHttpServerExit(event:NativeProcessExitEvent):void {
			var process:NativeProcess = NativeProcess(event.target)
			process.removeEventListener(NativeProcessExitEvent.EXIT, onStartHttpServerExit)
			httpServerStatus = HTTP_SERVER_DOWN
				
			process = null;
		}
	}
}