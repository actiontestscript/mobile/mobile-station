package com.ats.tools {
	import com.ats.helpers.NetworkUtils;
	import com.ats.helpers.PortSwitcher;
	import com.ats.helpers.Settings;
	import com.coltware.airxzip.ZipEntry;
	import com.coltware.airxzip.ZipError;
	import com.coltware.airxzip.ZipFileReader;
	
	import flash.desktop.NativeProcess;
	import flash.desktop.NativeProcessStartupInfo;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.IOErrorEvent;
	import flash.events.NativeProcessExitEvent;
	import flash.events.ProgressEvent;
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.net.URLStream;
	import flash.utils.ByteArray;
	
	public class ChromeDriverHelper extends EventDispatcher {
		
		private static const GOOGLE_APIS:String = "https://chromedriver.storage.googleapis.com"
		private static const LATEST_RELEASE:String = GOOGLE_APIS + "/LATEST_RELEASE"
		
		private static const URL_BASE:String = "/wd/ats";
		private static const CHROMEDRIVER_PREFIX:String = "chromedriver_";
		
		private var process:NativeProcess
		
		private var port:int = -1
		public var version:String = "N/A";
		
		public function launch():void{
			checkDriverVersion();
		}
		
		public function terminate():void{
			if(process != null && process.running){
				process.exit(true);
			}
		}
		
		private function get executableFile():File {
			return Settings.driversFolder.resolvePath(CHROMEDRIVER_PREFIX + version + (Settings.isMacOs ? "" : ".exe"))
		}
		
		//
		// - Process
		
		private function start():void {
			if (!executableFile.exists) {
				checkDriverVersion()
				return
			}

			if (Settings.isMacOs) {
				var processInfo:NativeProcessStartupInfo = new NativeProcessStartupInfo()
				processInfo.executable = new File("/bin/chmod")
				processInfo.arguments = new <String>["+x", executableFile.nativePath]

				var process:NativeProcess = new NativeProcess();
				process.addEventListener(NativeProcessExitEvent.EXIT, onChmodExit, false, 0, true);
				process.start(processInfo);
			} else {
				aaa()
			}
		}

		private function onChmodExit(event:NativeProcessExitEvent):void {
			aaa()
		}

		private function aaa():void {
			/* var availablePort:int = PortSwitcher.getAvailableLocalPort()

			var arguments:Vector.<String> = new <String>[]
			arguments.push("--port=" + availablePort)
			arguments.push("--url-base=" + URL_BASE)
			arguments.push("--whitelisted-ips=")

			var nativeProcessStartupInfo:NativeProcessStartupInfo = new NativeProcessStartupInfo()
			nativeProcessStartupInfo.executable = executableFile
			nativeProcessStartupInfo.arguments = arguments

			port = availablePort

			process = new NativeProcess()
			configureListeners(process)
			process.start(nativeProcessStartupInfo) */

			dispatchEvent(new Event(Event.COMPLETE))
		}

		public function stop():void {
			process.exit(true)
			process = null
		}
		
		private function removeListeners():void {
			process.removeEventListener(ProgressEvent.STANDARD_OUTPUT_DATA, process_standardOutputDataHandler)
			process.removeEventListener(ProgressEvent.STANDARD_ERROR_DATA, process_standardErrorDataHandler)
			process.removeEventListener(NativeProcessExitEvent.EXIT, process_exitHandler)
		}
		
		private function configureListeners(process:NativeProcess):void {
			process.addEventListener(ProgressEvent.STANDARD_OUTPUT_DATA, process_standardOutputDataHandler, false, 0, true)
			process.addEventListener(ProgressEvent.STANDARD_ERROR_DATA, process_standardErrorDataHandler, false, 0, true)
			process.addEventListener(NativeProcessExitEvent.EXIT, process_exitHandler, false, 0, true)
		}
		
		// todo: write chrome driver logs
		private function process_standardOutputDataHandler(event:ProgressEvent):void {
			trace(event.currentTarget.standardOutput.readUTFBytes(event.currentTarget.standardOutput.bytesAvailable))
		}
		
		private function process_standardErrorDataHandler(event:ProgressEvent):void {
			trace(event.currentTarget.standardOutput.readUTFBytes(event.currentTarget.standardOutput.bytesAvailable))
		}
		
		private function process_exitHandler(event:NativeProcessExitEvent):void {
			removeListeners()
			port = -1
		}
		
		public function get url():String{
			return NetworkUtils.getClientLocalIpAddress() + ":" + port + URL_BASE;
		}
		
		// -------
		
		private function checkDriverVersion():void {
			var loader:URLLoader = new URLLoader()
			loader.addEventListener(Event.COMPLETE, completeHandler)
			loader.addEventListener(IOErrorEvent.IO_ERROR, ioErrorHandler);
			
			var request:URLRequest = new URLRequest(LATEST_RELEASE)
			loader.load(request)
		}
		
		private function ioErrorHandler(ev:IOErrorEvent):void{
			var loader:URLLoader = ev.currentTarget as URLLoader;
			loader.removeEventListener(Event.COMPLETE, completeHandler)
			loader.removeEventListener(IOErrorEvent.IO_ERROR, ioErrorHandler)

			if(Settings.driversFolder.exists){
				var currentFile:File;
				for each(var f:File in Settings.driversFolder.getDirectoryListing()){
					if(f.name.indexOf(CHROMEDRIVER_PREFIX) == 0){
						if(currentFile == null || f.creationDate > currentFile.creationDate){
							currentFile = f;
						}
					}
				}
				
				if(currentFile != null){
					version = currentFile.name.replace(CHROMEDRIVER_PREFIX, "");
					if(currentFile.extension != null){
						version = version.substr(0, version.length - currentFile.extension.length - 1);
					}
					start();
					return;
				}
			}

			dispatchEvent(new Event(Event.COMPLETE));
		}
		
		private function completeHandler(event:Event):void {
			var loader:URLLoader = URLLoader(event.target);
			loader.removeEventListener(Event.COMPLETE, completeHandler)
			loader.removeEventListener(IOErrorEvent.IO_ERROR, ioErrorHandler)
			
			version = loader.data
			
			if (executableFile.exists) {
				start()
			} else {
				downloadDriver(loader.data)
			}
		}
		
		private var fileStream:FileStream = new FileStream()
		private var temporaryFile:File

		private function downloadDriver(version:String):void {
			var urlStream:URLStream = new URLStream()
			urlStream.addEventListener(Event.COMPLETE, urlStream_completeHandler);
			urlStream.addEventListener(Event.OPEN, urlStream_openHandler);
			urlStream.addEventListener(ProgressEvent.PROGRESS, urlStream_progressHandler);
			urlStream.addEventListener(IOErrorEvent.IO_ERROR, urlStream_ioErrorHandler);

			// todo: add macos support
			var request:URLRequest
			if (Settings.isMacOs) {
				if (Settings.isSilicon) {
					request = new URLRequest(GOOGLE_APIS + '/' + version + '/chromedriver_mac_arm64.zip')
				} else {
					request = new URLRequest(GOOGLE_APIS + '/' + version + '/chromedriver_mac64.zip')
				}
			} else {
				request = new URLRequest(GOOGLE_APIS + '/' + version + '/chromedriver_win32.zip')
			}

			try {
				urlStream.load(request)
			} catch (error:Error) {
				trace("Unable to load requested URL.");
				urlStream.removeEventListener(Event.COMPLETE, urlStream_completeHandler);
				urlStream.removeEventListener(Event.OPEN, urlStream_openHandler);
				urlStream.removeEventListener(ProgressEvent.PROGRESS, urlStream_progressHandler);
				dispatchEvent(new Event(Event.COMPLETE));
			}
		}
		
		// - URLStream events
		
		private function urlStream_completeHandler(event:Event):void {
			var urlStream:URLStream = URLStream(event.target)
			urlStream.removeEventListener(Event.COMPLETE, urlStream_completeHandler);
			urlStream.removeEventListener(Event.OPEN, urlStream_openHandler);
			urlStream.removeEventListener(ProgressEvent.PROGRESS, urlStream_progressHandler);
			
			fileStream.close()
			
			var reader:ZipFileReader = new ZipFileReader()
			reader.open(temporaryFile)
			var list:Array = reader.getEntries()
			for each (var entry:ZipEntry in list) {
				// todo: add macos support
				var execName:String = Settings.isMacOs ? "chromedriver" : "chromedriver.exe"
				if (entry.getFilename() == execName) {
					try {
						var bytes:ByteArray = reader.unzip(entry)
						fileStream = new FileStream()
						fileStream.open(executableFile, FileMode.WRITE)
						fileStream.writeBytes(bytes, 0, bytes.length)
						fileStream.close()
					} catch (e:ZipError) {
						// log.warn(entry.getFilename() + ":" + e.message);
						dispatchEvent(new Event(Event.COMPLETE));
					}
				}
			}
			reader.close()
			
			temporaryFile.deleteFile()
			temporaryFile = null
			fileStream = null
			
			start()
		}
		
		private function urlStream_openHandler(event:Event):void {
			temporaryFile = File.createTempFile()
			
			fileStream = new FileStream()
			fileStream.open(temporaryFile, FileMode.WRITE)
		}
		
		private function urlStream_progressHandler(event:ProgressEvent):void {
			var urlStream:URLStream = URLStream(event.target)
			
			var fileData:ByteArray = new ByteArray()
			urlStream.readBytes(fileData, 0, urlStream.bytesAvailable)
			fileStream.writeBytes(fileData,0,fileData.length)
		}

		private function urlStream_ioErrorHandler(event:IOErrorEvent):void {
			trace(event.text)
		}
	}
}