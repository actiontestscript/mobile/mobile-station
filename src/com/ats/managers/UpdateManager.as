package com.ats.managers {
    import com.ats.device.running.RunningDevice;
    import com.ats.helpers.Settings;
    import com.ats.helpers.Version;
    import com.ats.tools.Python;

    import flash.desktop.NativeApplication;
    import flash.events.ErrorEvent;

    import flash.events.Event;

    import flash.events.EventDispatcher;
    import flash.events.HTTPStatusEvent;
    import flash.events.IOErrorEvent;
    import flash.events.SecurityErrorEvent;
    import flash.events.TimerEvent;
    import flash.filesystem.File;
    import flash.filesystem.FileMode;
    import flash.filesystem.FileStream;
    import flash.net.URLLoader;
    import flash.net.URLRequest;
    import flash.net.URLStream;
    import flash.utils.ByteArray;
    import flash.utils.Timer;

    import mx.collections.ArrayCollection;

    import mx.core.FlexGlobals;

    import mx.utils.UIDUtil;

    public class UpdateManager extends EventDispatcher {

        public static const DEFAULT:String = "default"
        public static const UPDATE_AVAILABLE:String = "updateAvailable"
        public static const UPDATING:String = "updating"
        public static const UPDATE_COMPLETE:String = "updateComplete"

        private static const SERVER_URL:String = "https://www.actiontestscript.org"

        private static const DEFAULT_DELAY:Number =  3600000; // once an hour

        [Bindable]
        public var stream:URLStream

        [Bindable]
        public var currentVersion:Version

        [Bindable]
        public var newVersion:Version

        [Bindable]
        public var status:String = DEFAULT

        private var timer:Timer
        private var downloadUrl:String
        private var tempFile:File

        [Bindable]
        public var error:String

        public static var shared:UpdateManager = new UpdateManager()

        public function UpdateManager() { }

        public static function getInstance():UpdateManager {
            return shared
        }

        private static function get urlServer():String {
            return Settings.getInstance().customServer ? "https://" + Settings.getInstance().customServerAddress : SERVER_URL
        }

        public function setup(version:Version):void {
            currentVersion = version

            timer = new Timer(DEFAULT_DELAY, 0)
            timer.addEventListener(TimerEvent.TIMER, timer_timerHandler);
            timer.addEventListener(TimerEvent.TIMER_COMPLETE, timer_timerCompleteHandler);
        }

        public function start():void {
            if (status == DEFAULT) {
                check()
                timer.start()
            }
        }

        public function stop():void {
            timer.stop()

            // removeListeners(timer)
            // removeListeners(loader)
        }

        /* -- TIMER EVENTS -- */

        private function timer_timerHandler(event:TimerEvent):void {
            check()
        }

        private function timer_timerCompleteHandler(event:TimerEvent):void {
            start()
        }

        private function check():void {
            var request:URLRequest = new URLRequest(urlServer + "/mobile.php?os=" + Settings.osName + "&" + UIDUtil.createUID())
            var loader:URLLoader = new URLLoader(request)
            loader.addEventListener(IOErrorEvent.IO_ERROR, loader_ioErrorHandler);
            loader.addEventListener(Event.COMPLETE, loader_completeHandler);
            loader.load(request)
        }

        /* -- URLLOADER EVENTS -- */

        private function loader_completeHandler(event:Event):void {
            var loader:URLLoader = URLLoader(event.target)

            const data:String = loader.data

            try {
                var jsonData:Object = JSON.parse(data) as Object
            } catch (err:Error) {
                trace("ERROR : " + err.message)
            }

            if (jsonData == null || !jsonData.hasOwnProperty("release")) {
                trace("ERROR : BAD JSON")
                return
            }

            var version:Version = new Version(jsonData.release.version)
            trace(version.compare(currentVersion))
            if (version.compare(currentVersion) != Version.SUPERIOR) {
                return
            }

            timer.stop()

            tempFile = File.cacheDirectory.resolvePath("AtsMobileStation_" + version.stringValue + ".zip")
            newVersion = version
            downloadUrl = urlServer + "/" + jsonData.release.path

            if (Settings.getInstance().automaticUpdateEnabled) {
                download()
            } else {
                status = UPDATE_AVAILABLE
                dispatchEvent(new Event(status))
            }
        }

        public function download():void {
            var tempFile:File = File.cacheDirectory.resolvePath("AtsMobileStation_" + newVersion.stringValue + ".zip")
            if (tempFile.exists) {
                status = UPDATE_COMPLETE
                dispatchEvent(new Event(status))
                return
            }

            status = UPDATING
            dispatchEvent(new Event(status))

            stream = new URLStream()
            stream.addEventListener(Event.COMPLETE, stream_completeHandler)
            stream.addEventListener(HTTPStatusEvent.HTTP_STATUS, stream_httpStatusHandler)
            stream.addEventListener(IOErrorEvent.IO_ERROR, stream_ioErrorHandler)
            stream.addEventListener(Event.OPEN, stream_openHandler);
            stream.addEventListener(SecurityErrorEvent.SECURITY_ERROR, stream_securityErrorHandler);

            var request:URLRequest = new URLRequest(downloadUrl)
            stream.load(request)
        }


        // STREAM EVENTS

        private function stream_completeHandler(event:Event):void {
            var urlStream:URLStream = URLStream(event.target)
            urlStream.removeEventListener(Event.COMPLETE, stream_completeHandler)

            var data:ByteArray = new ByteArray()
            urlStream.readBytes(data)
            urlStream.close()

            var fileStream:FileStream = new FileStream()
            fileStream.addEventListener(Event.CLOSE, fileStreamCloseHandler, false, 0, true)

            try {
                fileStream.openAsync(tempFile, FileMode.WRITE)
                fileStream.writeBytes(data)
            } catch (e:Error) {
            } finally {
                fileStream.close()
            }
        }

        private function fileStreamCloseHandler(event:Event):void {
            var fileStream:FileStream = FileStream(event.target)
            fileStream.removeEventListener(Event.CLOSE, fileStreamCloseHandler)

            status = UPDATE_COMPLETE
            dispatchEvent(new Event(UPDATE_COMPLETE))
        }

        private function stream_httpStatusHandler(event:HTTPStatusEvent):void {

        }

        private function stream_ioErrorHandler(event:IOErrorEvent):void {

        }

        private function stream_openHandler(event:Event):void {

        }

        private function stream_securityErrorHandler(event:SecurityErrorEvent):void {

        }

        public function install():void {
            if (!canInstall()) {
                trace("ERROR : test in progress...")
                // pop up are you sure ?
            } else {
                FlexGlobals.topLevelApplication.closeAll()
                NativeApplication.nativeApplication.exit();

                var python:Python = new Python()
                python.updateApp(tempFile, FlexGlobals.topLevelApplication.appName)
            }
        }

        private static function canInstall():Boolean {
            var devices:ArrayCollection = FlexGlobals.topLevelApplication.devices.collection
            for each (var device:RunningDevice in devices) {
                if (device.locked) {
                    return false
                }
            }

            return true
        }

        private function loader_ioErrorHandler(event:IOErrorEvent):void {
            error = event.text
            status = ErrorEvent.ERROR
            dispatchEvent(new ErrorEvent(ErrorEvent.ERROR, false, false, event.text))
        }
    }
}
