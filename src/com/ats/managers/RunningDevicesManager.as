package com.ats.managers {
    import com.ats.device.Device;
    import com.ats.device.running.RunningDevice;
    import com.ats.device.running.android.AndroidDevice;
    import com.ats.device.running.ios.IosDevice;
    import com.ats.device.running.ios.IosDeviceInfo;
    import com.ats.device.simulator.IosSimulator;
    import com.ats.device.simulator.Simulator;
    import com.ats.helpers.Settings;
    import com.greensock.TweenLite;
    import com.greensock.TweenMax;
    
    import flash.desktop.NativeProcess;
    import flash.desktop.NativeProcessStartupInfo;
    import flash.events.Event;
    import flash.events.EventDispatcher;
    import flash.events.NativeProcessExitEvent;
    import flash.events.ProgressEvent;
    import flash.filesystem.File;
    import flash.filesystem.FileMode;
    import flash.filesystem.FileStream;
    import flash.net.URLLoader;
    import flash.net.URLLoaderDataFormat;
    import flash.net.URLRequest;

    import mx.collections.ArrayCollection;
    import mx.core.FlexGlobals;
    import mx.events.CollectionEvent;
    import mx.utils.UIDUtil;
    import mx.utils.URLUtil;

    public class RunningDevicesManager extends EventDispatcher {

        public static const INSTALL_COMPLETE:String = "installComplete"

        private const sysprofilerArgs:Vector.<String> = new <String>["system_profiler", "SPUSBDataType", "-json"];
        private const adbListDevicesArgs:Vector.<String> = new <String>["devices", "-l"];

        public static const endOfMessage:String = "<$ATSDROID_endOfMessage$>";
        // private const iosDevicePattern:RegExp = /(.*)\(([^\)]*)\).*\[(.*)\](.*)/;
        // private const jsonPattern:RegExp = /\{[^]*\}/;
        public static const responseSplitter:String = "<$atsDroid_ResponseSPLIITER$>";

        public static var devTeamId:String = "";
		
		private var iosProcInfo:NativeProcessStartupInfo = new NativeProcessStartupInfo();
		private var adbProcInfo:NativeProcessStartupInfo = new NativeProcessStartupInfo();
		
        private static function isSimulator(info:Array):Boolean {
            var isSimulator:Boolean = false
            for each (var line:String in info) {
                if (line.indexOf("device:") == 0) {
                    var deviceName:String = line.replace("device:", "")
                    return deviceName.indexOf("generic") == 0 || deviceName.indexOf("vbox") == 0
                }
            }
            return isSimulator
        }

        public function RunningDevicesManager() {
			adbProcInfo.workingDirectory = File.userDirectory;
			
            if (Settings.isMacOs) {

				adbProcInfo.executable = Settings.workAdbFolder.resolvePath("adb");
				
				iosProcInfo.executable = new File("/bin/chmod")
				iosProcInfo.workingDirectory = File.applicationDirectory.resolvePath("assets/tools")
				iosProcInfo.arguments = new <String>["+x", "android/platform-tools/adb", "ios/mobiledevice"]

                var proc:NativeProcess = new NativeProcess();
                proc.addEventListener(NativeProcessExitEvent.EXIT, onChmodExit, false, 0, true);
                proc.start(iosProcInfo);

            } else {
				adbProcInfo.executable = Settings.workAdbFolder.resolvePath("adb.exe");
				killAdbServer(onKillAdbServerAtStart);
            }
        }
		
		protected function onChmodExit(ev:NativeProcessExitEvent):void {
			ev.target.removeEventListener(NativeProcessExitEvent.EXIT, onChmodExit);
			ev.target.closeInput();
			
			iosProcInfo.executable = new File("/usr/bin/env");
			iosProcInfo.workingDirectory = File.userDirectory;
			
			killAdbServer(onKillAdbServerAtStartMacOs);
		}
		
		private function onKillAdbServerAtStart(ev:NativeProcessExitEvent):void {
			ev.target.removeEventListener(NativeProcessExitEvent.EXIT, onKillAdbServerAtStart);
			
			adbLoop = TweenLite.delayedCall(3, launchAdbProcess);
		}
		
		private function onKillAdbServerAtStartMacOs(ev:NativeProcessExitEvent):void {
			ev.target.removeEventListener(NativeProcessExitEvent.EXIT, onKillAdbServerAtStartMacOs);
			
			adbLoop = TweenLite.delayedCall(3, launchAdbProcess);
			iosLoop = TweenLite.delayedCall(3, launchIosProcess);
		}

        [Bindable]
        public var collection:ArrayCollection = new ArrayCollection();
		
        private var androidOutput:String;
        private var iosOutput:String;
        private var adbLoop:TweenLite;
        private var iosLoop:TweenLite;
        private var usbDevicesIdList:Vector.<String>;
		private var isInstalling:Boolean = false

        public function terminate():void {

            if (adbLoop) {
                adbLoop.pause();
                TweenLite.killDelayedCallsTo(launchAdbProcess)
            }

			if (iosLoop != null) {
                iosLoop.pause();
                TweenLite.killDelayedCallsTo(launchIosProcess)
            }

            var dv:RunningDevice;
            for each(dv in collection) {
                dv.close();
            }

			killAdbServer(onKillAdbServerAtExit);
        }
		
		private function killAdbServer(exitFunc:Function):void{

			adbProcInfo.arguments = new <String>["kill-server"];
			
			var proc:NativeProcess = new NativeProcess();
			proc.addEventListener(NativeProcessExitEvent.EXIT, exitFunc, false, 0, true);
			proc.start(adbProcInfo);
		}

        public function restartDev(dev:Device):void {
            dev.close();
        }

        public function findDevice(id:String):RunningDevice {
            for each(var dv:RunningDevice in collection) {
                if (dv.id == id) {
                    return dv;
                }
            }
            return null;
        }
		
        private function launchAdbProcess():void {
            androidOutput = ""
			adbProcInfo.arguments = adbListDevicesArgs;

			var adbProc:NativeProcess = new NativeProcess();
			adbProc.addEventListener(NativeProcessExitEvent.EXIT, onReadAndroidDevicesExit, false, 0, true);
			adbProc.addEventListener(ProgressEvent.STANDARD_OUTPUT_DATA, onReadAndroidDevicesData, false, 0, true);
			adbProc.start(adbProcInfo);
        }

        private function getDevicesIds(itmList:Object):void {
            for each (var object:Object in itmList) {
                if (object.hasOwnProperty("_items")) {
                    getDevicesIds(object._items)
                } else {
                    const name:String = object._name.toString().toLowerCase();
                    if (name == "iphone") {
                        var serialNumber:String = object.serial_num
                        if (serialNumber.length == 24) {
                            serialNumber = serialNumber.slice(0, 8) + "-" + serialNumber.slice(8);
                        }

                        usbDevicesIdList.push(serialNumber);
                    }
                }
            }
        }

        private function launchIosProcess():void {
            iosOutput = String("");
            iosProcInfo.arguments = sysprofilerArgs;

			var proc:NativeProcess = new NativeProcess();
            proc.addEventListener(NativeProcessExitEvent.EXIT, onUsbDeviceExit);
            proc.addEventListener(ProgressEvent.STANDARD_OUTPUT_DATA, onReadIosDevicesData);
            proc.start(iosProcInfo);
        }

        private function loadDevicesId():void {
            if (usbDevicesIdList.length > 0) {

                var id:String = usbDevicesIdList.pop();
                const dev:RunningDevice = findDevice(id) as IosDevice;

                if (dev == null) {
                    var devInfo:IosDeviceInfo = new IosDeviceInfo(id);
                    devInfo.addEventListener(Event.COMPLETE, realDevicesInfoLoaded, false, 0, true);
                    devInfo.load();
                } else {
                    loadDevicesId()
                }
            } else {
                realDevicesLoaded();
            }
        }

        private function realDevicesLoaded():void {
            for each(var sim:Simulator in FlexGlobals.topLevelApplication.simulators.collection) {
                if (sim is IosSimulator) {
                    if (sim.started) {
                        var dev:IosDevice = findDevice(sim.id) as IosDevice;
                        if (dev == null) {
                            dev = (sim as IosSimulator).GetDevice;
                            dev.start();
                            dev.addEventListener(Device.STOPPED_EVENT, deviceStoppedHandler, false, 0, true);

                            collection.addItem(dev);
                            collection.refresh();
                        }
                    }
                }
            }

            iosLoop.restart(true);
        }

        public function deviceStoppedHandler(event:Event):void {
            var closedDevice:RunningDevice = RunningDevice(event.target)
            closedDevice.removeEventListener(Device.STOPPED_EVENT, deviceStoppedHandler)

            collection.removeItem(closedDevice)
            collection.refresh();
        }

        //---------------------------------------------------------------------------------------------------------
        //---------------------------------------------------------------------------------------------------------


        protected function onReadAndroidDevicesData(ev:ProgressEvent):void {
            const len:int = ev.target.standardOutput.bytesAvailable;
            const data:String = ev.target.standardOutput.readUTFBytes(len);
            androidOutput += data
        }

		private var runningIds:Vector.<String>;
        protected function onReadAndroidDevicesExit(ev:NativeProcessExitEvent):void {
            ev.target.removeEventListener(NativeProcessExitEvent.EXIT, onReadAndroidDevicesExit);
            ev.target.removeEventListener(ProgressEvent.STANDARD_OUTPUT_DATA, onReadAndroidDevicesData);
            //ev.target.closeInput();

            //------------------------------------------------------------------------------------------

            var data:Array = androidOutput.split("\n");
            runningIds = new Vector.<String>();
			
			for each (var line:String in data) {
				if (line.length > 0) {
					var deviceInfo:Array = line.match(/([^ ]*)\s*device (.*)/);
					if (deviceInfo != null) {
						runningIds.push(deviceInfo[1]);
					}
				}
			}
			
			for each (var androidDev:RunningDevice in collection) {
				if (androidDev is AndroidDevice && !androidDev.simulator && runningIds.indexOf(androidDev.id) < 0) {
					androidDev.close()
				}
			}
			
			launchAndroidDevice();
        }
		
		private function launchAndroidDevice():void{
			if(runningIds.length > 0){
				
				var id:String = runningIds.removeAt(0) as String;
				
				var isEmulator:Boolean = false //TODO check is emulator
				var dev:RunningDevice = findDevice(id);
				
				if (dev != null) {
					if (dev.status == Device.FAIL) {
						dev.close();
					} else if (dev.status == Device.BOOT) {
						dev.start();
					}
				} else {
					dev = AndroidDevice.setup(id, isEmulator);
					dev.addEventListener(Device.STOPPED_EVENT, deviceStoppedHandler, false, 0, true);
					dev.start();
					
					collection.addItem(dev);
					collection.refresh();
				}
				
				TweenMax.delayedCall(1.0, launchAndroidDevice);
				
			}else{
				adbLoop.restart(true);
			}
		}

        private function onKillAdbServerAtExit(ev:NativeProcessExitEvent):void {
            ev.target.removeEventListener(NativeProcessExitEvent.EXIT, onKillAdbServerAtExit);
            dispatchEvent(new Event(Event.COMPLETE));
        }

        private function onReadIosDevicesData(ev:ProgressEvent):void {
            const len:int = ev.target.standardOutput.bytesAvailable;
            const data:String = ev.target.standardOutput.readUTFBytes(len);
            iosOutput = String(iosOutput.concat(data));
        }

        private function onUsbDeviceExit(ev:NativeProcessExitEvent):void {
            ev.target.removeEventListener(NativeProcessExitEvent.EXIT, onUsbDeviceExit);
            ev.target.removeEventListener(ProgressEvent.STANDARD_OUTPUT_DATA, onReadIosDevicesData);
            ev.target.closeInput();

            //------------------------------------------------------------------------------------------

            usbDevicesIdList = new <String>[]

            const json:Object = JSON.parse(iosOutput)
            getDevicesIds(json["SPUSBDataType"])

            for each(var iosDev:RunningDevice in collection) {
                if (iosDev is IosDevice && !iosDev.simulator && usbDevicesIdList.indexOf(iosDev.id) == -1) {
                    iosDev.close()
                }
            }

            loadDevicesId();
        }

        private function realDevicesInfoLoaded(ev:Event):void {

            ev.target.removeEventListener(Event.COMPLETE, realDevicesInfoLoaded);

            var dev:IosDevice = ev.target.device;
            dev.addEventListener(Device.STOPPED_EVENT, deviceStoppedHandler, false, 0, true);

            collection.addItem(dev);
            collection.refresh();

            dev.start();

            loadDevicesId()
        }

		// ----------------------------------- //
		// ----------- INSTALL APP ----------- //
		// ----------------------------------- //

        private var appName:String
        private var deviceIds:ArrayCollection
		public function installApp(url:String, target:String, deviceIds:Array):void {
            if (isInstalling) {
                trace("Install app error: ")
                return
            }

            this.deviceIds = new ArrayCollection(deviceIds)

            if (URLUtil.isHttpsURL(url) || URLUtil.isHttpURL(url)) {
                appName = UIDUtil.createUID()
                if (target == "android") {
                    appName += ".apk"
                } else if (target == "ios") {
                    appName += ".ipa"
                } else if (target == "app") {
                    appName += ".app"
                } else {
                    trace("Install app error: unknow target")
                    return
                }

                isInstalling = true
                downloadAppFile(url)
            } else {
                try {
                    var file:File = new File(url)
                } catch (error:ArgumentError) {
                    trace("Install app error: " + error.message)
                    return
                }

                if (file.exists) {
                    isInstalling = true
                    installAppFile(file)
                } else {
                    trace("Install app error: file doesn't exist")
                }
            }
		}

        public function installAppFile(file:File):void {
            collection.addEventListener(CollectionEvent.COLLECTION_CHANGE, deviceChangeHandler, false, 0, true)

            if (file.extension == "apk") {
                for each(var device:RunningDevice in collection) {
                    if (device is AndroidDevice) {
                        device.installLocalFile(file)
                    }
                }
            } else {
                installIosFile(file)
            }
        }

        public function installIosFile(file:File):void {
            var info:NativeProcessStartupInfo = new NativeProcessStartupInfo()
            info.executable = new File("/usr/bin/env")
            info.arguments = new <String>["cfgutil", "install-app", file.nativePath]

            var process:NativeProcess = new NativeProcess()
            process.addEventListener(NativeProcessExitEvent.EXIT, onInstallIosAppExit, false, 0, true);
            process.start(info)
        }

        private function onInstallIosAppExit(event:NativeProcessExitEvent):void {
            (event.target as NativeProcess).removeEventListener(NativeProcessExitEvent.EXIT, onInstallIosAppExit)

            dispatchEvent(new Event(RunningDevicesManager.INSTALL_COMPLETE))
        }

        private function downloadAppFile(url:String):void {
            var urlRequest:URLRequest = new URLRequest(url)
            var urlLoader:URLLoader = new URLLoader();
            urlLoader.dataFormat = URLLoaderDataFormat.BINARY
            urlLoader.addEventListener(Event.COMPLETE, onAppDownloadComplete, false, 0, true);
            urlLoader.load(urlRequest)
        }

        internal var temporaryAppFile:File
        private function onAppDownloadComplete(event:Event):void {
            var urlLoader:URLLoader = URLLoader(event.target)
            urlLoader.removeEventListener(Event.COMPLETE, onAppDownloadComplete)

            var fileStream:FileStream = new FileStream();
            temporaryAppFile = File.cacheDirectory.resolvePath(appName)
            fileStream.addEventListener(Event.CLOSE, writeFileComplete);
            try {
                fileStream.openAsync(temporaryAppFile, FileMode.WRITE);
                fileStream.writeBytes(urlLoader.data, 0, urlLoader.data.length);
            } catch (e:Error) {

            } finally {
                fileStream.close();
            }
        }

        private function writeFileComplete(ev:Event):void {
            var fileStream:FileStream = ev.currentTarget as FileStream;
            fileStream.removeEventListener(Event.CLOSE, writeFileComplete);

            installAppFile(temporaryAppFile)
        }

        private function deviceChangeHandler(event:CollectionEvent):void {
            var count:int = 0
            for each(var device:RunningDevice in collection) {
                if (device.status == Device.INSTALL_APP) {
                    count++
                }
            }

            if (count == 0) {
                trace("end installing")
                isInstalling = false
                collection.removeEventListener(CollectionEvent.COLLECTION_CHANGE, deviceChangeHandler)

                if (temporaryAppFile) {
                    temporaryAppFile.deleteFile()
                    temporaryAppFile = null
                }

                dispatchEvent(new Event(INSTALL_COMPLETE));
            } else {
                trace("installing.....")
                isInstalling = true
            }
        }
    }
}