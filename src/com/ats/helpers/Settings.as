package com.ats.helpers {
	import flash.errors.IOError;
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	import flash.system.Capabilities;

	import mx.utils.UIDUtil;

	public class Settings {

		private static const APP_IDENTIFIER:String = "identifier"
		private static const AUTOMATIC_UPDATE:String = "automatic_update"
		private static const DEVELOPMENT_TEAM_ID:String = "development_team"
		private static const WEB_SERVER_PORT:String = "web_server_port"
		private static const CUSTOM_SERVER_ADDRESS:String = "custom_server_address"
		private static const CUSTOM_SERVER:String = "custom_server"

		private static const APP_FOLDER_WINDOWS:String = "AppData/Local";
		private static const APP_FOLDER_MACOS:String = "Library";

		private static const settingsFolder:File = File.userDirectory.resolvePath(".actiontestscript/mobilestation/settings");

		public static const isMacOs:Boolean = Capabilities.os.indexOf("Mac") > -1;

		public static var isSilicon:Boolean = false

		public static const workAdbFolder:File = File.applicationDirectory.resolvePath("assets/tools/android/platform-tools");
		public static const workFolder:File = File.userDirectory.resolvePath(".atsmobilestation");
		public static const driversFolder:File = workFolder.resolvePath("drivers");

		private static var _instance:Settings = new Settings();

		[Bindable]
		public var automaticUpdateEnabled:Boolean = true;

		[Bindable]
		public var appIdentifier:String

		[Bindable]
		public var appleDeveloperTeamId:String

		[Bindable]
		public var webServerPort:int = 9000;

		[Bindable]
		public var customServer:Boolean = false;

		[Bindable]
		public var customServerAddress:String

		public static function get defaultAppFolder():File {
			return isMacOs ? File.userDirectory.resolvePath(APP_FOLDER_MACOS) : File.userDirectory.resolvePath(APP_FOLDER_WINDOWS)
		}

		public static function get osName():String {
			return isMacOs ? "macos" : "windows"
		}

		public static function get logsFolder():File {
			return workFolder.resolvePath("logs");
		}

		public static function get httpFolder():File {
			return workFolder.resolvePath("http");
		}

		public static function get devicesSettingsFile():File {
			return settingsFolder.resolvePath("devicesSettings.txt");
		}

		public static function get settingsFile():File {
			return settingsFolder.resolvePath("settings.txt");
		}

		public static function get adbFile():File {
			return workAdbFolder.resolvePath("adb" + (isMacOs ? "" : ".exe"))
		}

		public static function getInstance():Settings {
			return _instance;
		}

		public static function cleanLogs():void {
			try {
				logsFolder.deleteDirectory(true);
			} catch (err:IOError) {
			}
		}

		public function Settings() {
			if (_instance) {
				throw new Error("Settings is a singleton and can only be accessed through Settings.getInstance()");
			}

			loadProperties()
		}

		public function loadProperties():void {
			var appIdentifier:String

			var file:File = settingsFile;
			var fileStream:FileStream = new FileStream();
			if (file.exists) {
				fileStream.open(file, FileMode.READ);

				var settingsContent:String = fileStream.readUTFBytes(fileStream.bytesAvailable);
				var settingsContentArray:Array = settingsContent.split("\n");

				for each (var info:String in settingsContentArray) {
					if (info) {
						var key:String = info.split("==")[0]
						var value:String = info.split("==")[1]
						if (isMacOs && key == DEVELOPMENT_TEAM_ID) {
							appleDeveloperTeamId = value
						} else if (key == APP_IDENTIFIER) {
							appIdentifier = value
						} else if (key == AUTOMATIC_UPDATE) {
							automaticUpdateEnabled = value == "true"
						} else if (key == WEB_SERVER_PORT) {
							webServerPort = parseInt(value)
						} else if (key == CUSTOM_SERVER) {
							customServer = value == "true"
						} else if (key == CUSTOM_SERVER_ADDRESS) {
							customServerAddress = value
						}
					}
				}
				fileStream.close();

				if (appIdentifier == null) {
					appIdentifier = UIDUtil.createUID()
					settingsContentArray.push(APP_IDENTIFIER + "==" + appIdentifier)

					fileStream.open(file, FileMode.WRITE)
					fileStream.writeUTFBytes(settingsContentArray.join("\n"))
					fileStream.close()
				}

			} else {
				fileStream.open(file, FileMode.WRITE)
				appIdentifier = UIDUtil.createUID()
				fileStream.writeUTFBytes(APP_IDENTIFIER + "==" + UIDUtil.createUID())
				fileStream.close()
			}

			this.appIdentifier = appIdentifier
		}

		public function save():void {
			var propertiesToSave:Vector.<String> = new Vector.<String>()
			propertiesToSave.push(APP_IDENTIFIER + "==" + appIdentifier)
			propertiesToSave.push(AUTOMATIC_UPDATE + "==" + automaticUpdateEnabled)
			propertiesToSave.push(WEB_SERVER_PORT + "==" + webServerPort)
			propertiesToSave.push(CUSTOM_SERVER + "==" + customServer)
			propertiesToSave.push(CUSTOM_SERVER_ADDRESS + "==" + customServerAddress)
			if (isMacOs) {
				propertiesToSave.push(DEVELOPMENT_TEAM_ID + "==" + appleDeveloperTeamId)
			}

			var fileStream:FileStream = new FileStream()
			fileStream.open(settingsFile, FileMode.WRITE)
			fileStream.writeUTFBytes(propertiesToSave.join("\n"))
			fileStream.close()
		}
	}
}