package com.ats.helpers {
	
	import flash.net.InterfaceAddress;
	import flash.net.NetworkInfo;
	import flash.net.NetworkInterface;
	
	public final class NetworkUtils {
		
		public static function getClientLocalIpAddress():String {
			var interfaces:Vector.<NetworkInterface> = NetworkInfo.networkInfo.findInterfaces()
			var ip:String = findLocalIpAddress(interfaces, checkClassCsubnet);
			if(ip == null){
				ip = findLocalIpAddress(interfaces, checkClassBsubnet);
			}
			return ip;
		}
		
		private static function findLocalIpAddress(interfaces:Vector.<NetworkInterface>, classTypeFunction:Function):String{
			
			var interfaceObj:NetworkInterface;
			var address:InterfaceAddress;
			
			for (var i:int = 0; i < interfaces.length; i++) {
				interfaceObj = interfaces[i];
				if (interfaceObj.active) {
					for (var j:int = 0; j < interfaceObj.addresses.length; j++) {
						address = interfaceObj.addresses[j]
						if (address.ipVersion == "IPv4" && address.address != null && address.broadcast != null) {
							if(classTypeFunction(address.address, address.broadcast)){
								return address.address;
							}
						}
					}
				}
			}
			return null;
		}
		
		private static function checkClassCsubnet(address1:String, address2:String):Boolean{
			var address1Data:Array = address1.split(".")
			var address2Data:Array = address2.split(".")
			return address1Data[0] == address2Data[0] && address1Data[1] == address2Data[1] && address1Data[2] == address2Data[2];
		}
		
		private static function checkClassBsubnet(address1:String, address2:String):Boolean{
			var address1Data:Array = address1.split(".")
			var address2Data:Array = address2.split(".")
			return address1Data[0] == address2Data[0] && address1Data[1] == address2Data[1];
		}
	}
}