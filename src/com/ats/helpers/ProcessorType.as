package com.ats.helpers {
import flash.system.Capabilities;

public final class ProcessorType {

    public static const MACOS_64:String = "mac64";
    public static const MACOS_64_M1:String = "mac64_m1";
    public static const WINDOWS_32:String = "win32";
    public static const WINDOWS_64:String = "win64";

}
}
