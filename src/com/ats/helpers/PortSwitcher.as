package com.ats.helpers
{
	import flash.net.ServerSocket;

	public class PortSwitcher
	{
		public static function generateAvailablePort(port:int):int {
			if (checkSocketPortAvailability(port)) {
				return port
			}

			var helper:DeviceSettingsHelper = DeviceSettingsHelper.shared
			while (!checkSocketPortAvailability(port) || !helper.assignedPortAvailable(port)) {
				port++;
			}

			return port
		}

		public static function checkSocketPortAvailability(port:int):Boolean {
			try {
				
				var server:ServerSocket = new ServerSocket();
				server.bind(port);
				var localPort:int = server.localPort;
				server.close();
				return localPort != 0;
				
			} catch (e:Error) {}
			
			return false;
		}

		public static function getAvailableLocalPort():int {
			var server:ServerSocket = new ServerSocket();
			server.bind(0, "127.0.0.1");
			var localPort:int = server.localPort;
			server.close();
			return localPort;
		}
	}
}