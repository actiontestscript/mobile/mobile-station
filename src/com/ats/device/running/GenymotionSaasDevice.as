package com.ats.device.running {
	import com.ats.device.running.android.AdbProcess;
	import com.ats.device.running.android.AdbProcessEvent;
	import com.ats.device.running.android.AndroidUsbDevice;
	import com.ats.helpers.DeviceSettings;
	import com.ats.helpers.DeviceSettingsHelper;

	public class GenymotionSaasDevice extends AndroidUsbDevice {

		private static const atsdroidRemoteFilePath:String = "http://actiontestscript.org/drivers/mobile/atsdroid.apk"
		private static var apkOutputPath:String

		public function GenymotionSaasDevice(id:String) {
			super(id, true, true, null);
		}

		override public function get modelName():String {
			return _modelName;
		}

		override public function checkBoot():void {
			trace("check boot")
			// generate fake name
			var name:String = adbIdentifier

			var settings:DeviceSettings =  DeviceSettingsHelper.shared.settingsForDevice(name)
			if (!settings) {
				settings = new DeviceSettings(name)
				settings.save()
			}
		    // load and set general settings
			automaticPort = settings.automaticPort

			// load and set port mapping
			port = settings.port.toString()

			super.checkBoot()
		}

		override public function get adbIdentifier():String {
			return ["GM", modelName, "v" + osVersion].join("_").replace(" ", "_")
		}

		public function saveName():String {
			return ["GM", modelName, "v" + osVersion].join("_").replace(" ", "_")
		}

		override protected function installDriver():void {
			installRemoteFile(atsdroidRemoteFilePath)
		}

		public override function installRemoteFile(url:String):void {
			var apkName:String = url.split("/").pop();
			printDebugLogs("Downloading " + apkName)
			apkOutputPath = "/sdcard/" + apkName

			// -q (quiet): delete output logs interpreted as errors
			// -O (output document): always overwrites file

			var adbProcess:AdbProcess = new AdbProcess(this)
			adbProcess.addEventListener(AdbProcessEvent.ADB_EXIT, apkDownloadComplete);
			adbProcess.execute(new <String>["-s", id, "shell", "wget", "-q", url, "-O", apkOutputPath])
		}

		private function apkDownloadComplete(ev:AdbProcessEvent):void {
			var adbProcess:AdbProcess = AdbProcess(ev.target)
			adbProcess.removeEventListener(AdbProcessEvent.ADB_EXIT, apkDownloadComplete);

			if (ev.error != null) {
				status = ERROR
				errorMessage = ev.error;
				return
			}

			adbProcess = new AdbProcess(this)
			adbProcess.addEventListener(AdbProcessEvent.ADB_EXIT, onInstallDriverExit)
			adbProcess.execute(new <String>["-s", id, "shell", "pm", "install", apkOutputPath])
		}
	}
}
