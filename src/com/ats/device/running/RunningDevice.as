package com.ats.device.running
{
	import avmplus.getQualifiedClassName;

	import com.ats.device.*;
	import flash.filesystem.File;

	import mx.utils.UIDUtil;

	[Bindable]
	[RemoteClass(alias="device.RunningDevice")]
	public class RunningDevice extends Device {
		public var locked:String = null;
		public var booted:Boolean = true;
		public var authorized:Boolean = true
		public var ip:String;
		public var port:String = "";

		public var runningId:String = UIDUtil.createUID();

		[Transient]public var startTime:Date
		[Transient]public var endTime:Date
		[Transient]public var duration:Number

		[Transient]public var automaticPort:Boolean = true;
		[Transient]public var error:String = null;
		[Transient]public var errorMessage:String;
		[Transient]public var usbMode:Boolean = false;
		[Transient]public var udpIpAddress:String;

		public function RunningDevice(id:String="") {
			super(id);
			startTime = new Date()

			trace("Starting driver -> " + getQualifiedClassName(this));
		}

		public function get type():String {
			if (simulator) {
				return (this is GenymotionSaasDevice) ? "GenymotionCloud" : "Simulator"
			} else {
				return "Physical"
			}
		}

		public function get osName():String {
			return null
		}

		public function get endpoint():String {
			return ip + ":" + port
		}

		public function start():void {
			tooltip = "Starting driver ..."
		}

		public function terminate():void { }

		protected function installing():void {
			status = INSTALL;
			tooltip = "Installing driver ..."
			trace("Installing driver -> " + getQualifiedClassName(this))
		}

		protected function started():void {
			status = READY;
			// trace("Driver started -> " + getQualifiedClassName(this));
			printDebugLogs("Driver started and ready");

			endTime = new Date()
			duration = endTime.time - startTime.time

			tooltip = "Driver started and ready (" + (duration / 1000).toFixed() + "s)";
		}

		protected function failed():void {
			status = FAIL;
			tooltip = "Driver can not be started";
			trace("Driver error -> " + getQualifiedClassName(this));
		}

		protected function usbError(error:String):void {
			status = USB_ERROR;
			errorMessage = " - " + error;
			tooltip = "Problem when installing driver ...";
			trace("USB install error -> " + getQualifiedClassName(this));
		}

		public function installRemoteFile(url:String):void {
			// do nothing by default
		}

		public function installLocalFile(file:File):void {
			// do nothing by default
		}

		protected function printDebugLogs(message:String):void {
			trace("[INFO][" + new Date().toString() + "]" + "[" + id + " | " + modelName + "]" + "[" + (usbMode ? "USB" : "WIFI") + "]" + " " + message)
		}
	}
}