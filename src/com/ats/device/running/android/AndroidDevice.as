package com.ats.device.running.android
{
	import com.ats.device.running.GenymotionSaasDevice;
	import com.ats.device.running.RunningDevice;
	import com.ats.helpers.DeviceSettings;
	import com.ats.helpers.DeviceSettingsHelper;
	import com.ats.helpers.Settings;
	import com.ats.helpers.Version;
	
	import flash.events.Event;
	import flash.events.ProgressEvent;
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	import flash.globalization.DateTimeFormatter;
	import flash.net.URLRequest;
	import flash.net.URLStream;
	import flash.utils.ByteArray;

	public class AndroidDevice extends RunningDevice {

		protected static const ANDROID_DRIVER:String = "com.ats.atsdroid";
		protected static const ANDROID_JUNIT_RUNNER:String = "/androidx.test.runner.AndroidJUnitRunner";
		private static const atsdroidFilePath:String = File.applicationDirectory.resolvePath("assets/drivers/atsdroid.apk").nativePath;

		[Transient]
		public var androidVersion:String
		[Transient]
		public var androidSdk:String

		private var _adbIdentifier:String;

		protected var launchTestProcess:AdbProcess

		private var logFile:File;

		//---------------------------------------------------------------------------------------------------------
		//---------------------------------------------------------------------------------------------------------

		private var logStream:FileStream = new FileStream();
		private var dateFormatter:DateTimeFormatter = new DateTimeFormatter("en-US");

		override public function get modelName():String {
			return simulator ? "Emulator " + _modelName : _modelName;
		}

		override public function get osName():String {
			return "Android"
		}

		public function AndroidDevice(id:String, simulator:Boolean) {
			this.id = _adbIdentifier = id;
			this.simulator = simulator;

			dateFormatter.setDateTimePattern("yyyy-MM-dd hh:mm:ss");
			logFile = Settings.logsFolder.resolvePath("android_" + id.replace(/[.:]/g, "") + "_" + new Date().time + ".log");
		}

		public static function setup(id:String, isEmulator:Boolean):AndroidDevice {
			if (id.indexOf("localhost") == 0) {
				return new GenymotionSaasDevice(id)
			} else {
				var deviceSettings:DeviceSettings = DeviceSettingsHelper.shared.settingsForDevice(id)
				if (!deviceSettings) {
					deviceSettings = new DeviceSettings(id)
					deviceSettings.save()
				}

				var automaticPort:Boolean = deviceSettings.automaticPort
				var usbMode:Boolean = deviceSettings.usbMode
				var port:int = deviceSettings.port

				if (usbMode) {
					return new AndroidUsbDevice(id, isEmulator, automaticPort, port.toString());
				} else {
					return new AndroidWirelessDevice(id, automaticPort, port.toString());
				}
			}
		}

		public override function start():void {
			super.start()

			writeInfoLogFile("Start Android process USB MODE = " + usbMode + " > set port: " + this.port);

			fetchDeviceInfo()
		}

		//---------------------------------------------------------------------------------------------------------
		// Download APK
		//---------------------------------------------------------------------------------------------------------

		private var apkFile:File;
		private var urlStream:URLStream;
		private var downloadedData:ByteArray;

		public override function installLocalFile(file:File):void {
			installing()

			var adbProcess:AdbProcess = new AdbProcess(this)
			adbProcess.addEventListener(AdbProcessEvent.ADB_EXIT, onInstallApkExit, false, 0, true);
			adbProcess.execute(new <String>["-s", id, "install", file.nativePath])
		}

		public override function installRemoteFile(url:String):void{
			status = INSTALL_APP

			printDebugLogs("Start apk download : " + url)

			downloadedData = new ByteArray();

			urlStream = new URLStream();
			urlStream.addEventListener(ProgressEvent.PROGRESS, streamProgress, false, 0, true);
			urlStream.addEventListener(Event.COMPLETE, streamComplete, false, 0, true);

			urlStream.load(new URLRequest(url));
		}

		private function streamProgress(ev:ProgressEvent):void{
			urlStream.readBytes(downloadedData, downloadedData.length, urlStream.bytesAvailable);
		}

		private function streamComplete(ev:Event):void{
			urlStream.removeEventListener(ProgressEvent.PROGRESS, streamProgress);
			urlStream.removeEventListener(Event.COMPLETE, streamComplete);

			printDebugLogs("Apk downloaded")

			urlStream.close();
			urlStream = null;

			apkFile = File.userDirectory.resolvePath(".atsmobilestation").resolvePath("temp").resolvePath("install.apk")
			if (apkFile.exists) {
				apkFile.deleteFile();
			}

			var fileStream:FileStream = new FileStream();
			fileStream.addEventListener(Event.CLOSE, writeFileComplete, false, 0, true);
			try {
				fileStream.openAsync(apkFile, FileMode.WRITE);
				fileStream.writeBytes(downloadedData);
			} catch (e:Error) {
			} finally {
				fileStream.close();
			}
		}

		private function writeFileComplete(ev:Event):void{
			var fileStream:FileStream = ev.currentTarget as FileStream;
			fileStream.removeEventListener(Event.CLOSE, writeFileComplete);

			var apkPath:String = apkFile.nativePath.replace(/\\/g, "/");
			printDebugLogs("Installing apk -> " + apkPath)

			var adbProcess:AdbProcess = new AdbProcess(this)
			adbProcess.addEventListener(AdbProcessEvent.ADB_EXIT, onInstallApkExit, false, 0, true);
			adbProcess.execute(new <String>["-s", id, "install", apkPath])
		}

		protected function onInstallApkExit(ev:AdbProcessEvent):void {
			ev.currentTarget.removeEventListener(AdbProcessEvent.ADB_EXIT, onInstallApkExit);
			printDebugLogs("Apk installed")
			started()
		}

		//---------------------------------------------------------------------------------------------------------
		//---------------------------------------------------------------------------------------------------------

		private static function getPropValue(value:String):String {
			return /.*:.*\[(.*)]/.exec(value)[1];
		}

		// to refactor -> regex
		private static function getDeviceOwner(data:String):String {
			var array:Array = data.split("\n");
			for each(var line:String in array) {
				if (line.indexOf("ATS_DRIVER_LOCKED_BY") > -1) {
					var firstIndex:int = line.length;
					var lastIndex:int = line.lastIndexOf("ATS_DRIVER_LOCKED_BY:") + "ATS_DRIVER_LOCKED_BY:".length;
					return line.substring(lastIndex, firstIndex).slice(0, -1);
				}
			}

			return null;
		}

		override public function terminate():void {
			if (launchTestProcess != null) {
				launchTestProcess.terminate();
			} else {
				close();
			}
		}

		public override function dispose():Boolean {
			if (launchTestProcess != null) {
				return launchTestProcess.terminate();
			} else {
				return false;
			}
		}

		public function writeErrorLogFile(data:String):void {
			writeLogs("ERROR", data);
		}

		public function writeInfoLogFile(data:String):void {
			writeLogs("INFO", data);
		}

		private function writeLogs(label:String, data:String):void {
			data = data.replace("INSTRUMENTATION_STATUS: atsLogs=", "");
			data = data.replace("INSTRUMENTATION_STATUS_CODE: 0", "");
			data = data.replace(/[\u000d\u000a\u0008]+/g, "");
			if (data.length > 0) {
				logStream.open(logFile, FileMode.APPEND);
				logStream.writeUTFBytes("[" + dateFormatter.format(new Date()) + "][" + label + "]" + data + "\n");
				logStream.close();
			}
		}

		//---------------------------------------------------------------------------------------------------------
		// Step 1 : fetch device info
		// Step 2 : fetch ip address
		// Step 3 : start chrome driver
		// Step 4 : uninstall apk driver
		// Step 5 : install apk driver
		// Step 6 : execute apk driver
		//---------------------------------------------------------------------------------------------------------

		private function fetchDeviceInfo():void {
			printDebugLogs("Fetching device info");

			var adbProcess:AdbProcess = new AdbProcess(this);
			adbProcess.addEventListener(AdbProcessEvent.ADB_EXIT, onDeviceInfoExit, false, 0, true);
			adbProcess.execute(new <String>["-s", id, "shell", "getprop"])
		}

		protected var bootInfo:String = "";

		private function onDeviceInfoExit(ev:AdbProcessEvent):void {
			ev.currentTarget.removeEventListener(AdbProcessEvent.ADB_EXIT, onDeviceInfoExit);

			if (ev.error != null) {
				status = ERROR
				trace("BOOT CHECK ERROR - " + id + " : " + ev.error)

				if (ev.error.indexOf("device unauthorized") != -1) {
					authorized = false
					error = "Device not authorized"
					errorMessage = "Check for a confirmation dialog on your device"
				} else if (ev.error.indexOf("device offline") != -1) {
					error = "Device not started"
					errorMessage = "Please wait until the device is started"
					booted = false
					status = BOOT
				} else {
					error = "Unknow error"
					errorMessage = "Please wait until the device is started"
				}

				return
			}

			var modelName:String
			for each (var line:String in ev.stackedOutput) {
				if (line.indexOf("[sys.boot_completed]") == 0) {
					bootInfo = getPropValue(line)
				} else if (line.indexOf("[ro.product.model]") == 0) {
					modelId = getPropValue(line)
				} else if (line.indexOf("[ro.build.version.release]") == 0) {
					osVersion = getPropValue(line)
				} else if (line.indexOf("[ro.build.version.sdk]") == 0) {
					androidSdk = getPropValue(line)
				} else if (line.indexOf("[ro.product.manufacturer]") == 0) {
					manufacturer = getPropValue(line)
				}

				if (simulator) {
					if (modelId.indexOf("GM") == 0) {
						var parameters:Array = modelId.split("_")
						modelName = parameters[1]
					} else {
						if (line.indexOf("[ro.product.cpu.abi]") == 0) {
							modelName = getPropValue(line)
						}
					}

				} else {
					if (line.indexOf("[ro.semc.product.name]") == 0) {
						modelName = getPropValue(line)
					} else if (line.indexOf("[def.tctfw.brandMode.name]") == 0) {
						modelName = getPropValue(line)
					}
				}
			}

			if (!modelName) modelName = modelId

			var myRegexPattern:RegExp = new RegExp(manufacturer + "\\s?", "i")
			this.modelName = modelName.replace(myRegexPattern, "");

			var deviceOsVersion:Version = new Version(osVersion)
			if (deviceOsVersion.compare(new Version("5.1")) == Version.INFERIOR) {
				status = ERROR
				error = "ANDROID_VERSION_NOT_SUPPORTED"
				errorMessage = "Only supports Android devices running version 5.1 or higher"
				return
			}

			checkBoot()
		}

		public function checkBoot():void {
			if (bootInfo && bootInfo == "1") {
				fetchIpAddress()
				status = INSTALL
				error = null
				errorMessage = null
			} else {
				error = "DEVICE_NOT_STARTED"
				errorMessage = "Please wait until the device is started ..."
				booted = false
				status = BOOT
			}
		}

		//---------------------------------------------------------------------------------------------------------
		//---------------------------------------------------------------------------------------------------------

		protected function fetchIpAddress():void {
			trace("WARNING : fetchIpAddress not implemented")
		}

		//---------------------------------------------------------------------------------------------------------
		// -------- DRIVER UNINSTALL
		//---------------------------------------------------------------------------------------------------------

		protected function uninstallDriver():void {
			writeInfoLogFile("Uninstall driver: ")

			var adbProcess:AdbProcess = new AdbProcess(this);
			adbProcess.addEventListener(AdbProcessEvent.ADB_EXIT, onUninstallDriverExit, false, 0, true);
			adbProcess.execute(new <String>["-s", id, "shell", "pm", "uninstall", ANDROID_DRIVER]);
		}

		protected function onUninstallDriverExit(ev:AdbProcessEvent):void {
			ev.currentTarget.removeEventListener(AdbProcessEvent.ADB_EXIT, onUninstallDriverExit);
			installDriver();
		}

		//---------------------------------------------------------------------------------------------------------
		// -------- DRIVER INSTALL
		//---------------------------------------------------------------------------------------------------------

		protected function installDriver():void {
			writeInfoLogFile("Install driver: ")

			var adbProcess:AdbProcess = new AdbProcess(this);
			adbProcess.addEventListener(AdbProcessEvent.ADB_EXIT, onInstallDriverExit, false, 0, true);
			adbProcess.execute(new <String>["-s", id, "install", "-r", atsdroidFilePath]);
		}

		protected function onInstallDriverError():void {
			// adbProcess.removeEventListener()
		}

		protected function onInstallDriverExit(ev:AdbProcessEvent):void {
			ev.currentTarget.removeEventListener(AdbProcessEvent.ADB_EXIT, onInstallDriverExit);

			if (ev.error != null && ev.error.indexOf("INSTALL_FAILED_USER_RESTRICTED") > -1) {
				error = "APK_INSTALL_FAIL";
				errorMessage = "Install of Ats driver failed, please check apk install restriction !"
				status = ERROR
			} else {
				executeDriver();
			}
		}

		//---------------------------------------------------------------------------------------------------------
		//-------- DRIVER EXECUTION
		//---------------------------------------------------------------------------------------------------------

		protected function startTestProcess(arguments:Vector.<String>):void{
			printDebugLogs("Starting driver");

			launchTestProcess = new AdbProcess(this);
			launchTestProcess.addEventListener(AdbProcessEvent.ADB_EXIT, onExecuteExit, false, 0, true);
			launchTestProcess.addEventListener(AdbProcessEvent.ADB_PROGRESS, onExecuteOutput, false, 0, true);
			launchTestProcess.addEventListener(AdbProcessEvent.ADB_ERROR, onExecuteError, false, 0, true);

			launchTestProcess.execute(arguments)
		}

		protected function executeDriver():void {
			trace("WARNING : executeDriver not implemented")
		}

		protected function onExecuteOutput(ev:AdbProcessEvent):void {

			writeErrorLogFile(ev.output);

			if (ev.output.indexOf("Process crashed") > -1) {
				launchTestProcess.injectCommand("instrumentCommandLine");
				return
			}

			if (ev.output.indexOf("ATS_DRIVER_RUNNING") > -1) {
				started()
			} else if (ev.output.indexOf("ATS_DRIVER_START") > -1) {
				trace("driver start -> " + ev.output);
			} else if (ev.output.indexOf("ATS_DRIVER_STOP") > -1) {
				trace("driver stop");
			} else if (ev.output.indexOf("ATS_WIFI_STOP") > -1) {
				// dispatchEvent(new Event(WIFI_ERROR_EVENT));
			} else if (ev.output.indexOf("ATS_DRIVER_LOCKED_BY:") > -1) {
				locked = getDeviceOwner(ev.output)
			} else if (ev.output.indexOf("ATS_DRIVER_UNLOCKED") > -1) {
				locked = null;
			}
		}

		protected function onExecuteError(ev:AdbProcessEvent):void {
			writeErrorLogFile(ev.error);
		}

		protected function onExecuteExit(ev:AdbProcessEvent):void {
			launchTestProcess.removeEventListener(AdbProcessEvent.ADB_EXIT, onExecuteExit);
			launchTestProcess.removeEventListener(AdbProcessEvent.ADB_PROGRESS, onExecuteOutput);
			launchTestProcess.removeEventListener(AdbProcessEvent.ADB_ERROR, onExecuteError);

			if (ev.error != null) {
				status = FAIL;
				trace("ATSDroid Execution error : " + ev.error);
				writeErrorLogFile("Failure on android process");
			} else {
				close();
			}

			logFile = null;
			launchTestProcess = null;
		}

		public function get adbIdentifier():String {
			return _adbIdentifier;
		}

	}
}