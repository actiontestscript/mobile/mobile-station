package com.ats.device.running.android {
	import com.adobe.utils.StringUtil;
	import com.ats.tools.ChromeDriverHelper;
	
	import mx.core.FlexGlobals;
	
	public class AndroidWirelessDevice extends AndroidDevice {
		
		public function AndroidWirelessDevice(id:String, automaticPort:Boolean, port:String) {
			super(id, false);
			
			this.automaticPort = automaticPort;
			this.usbMode = false;
			this.port = port
		}
		
		override protected function fetchIpAddress():void {
			var adbProcess:AdbProcess = new AdbProcess(this)
			adbProcess.addEventListener(AdbProcessEvent.ADB_EXIT, onReadLanExit);
			adbProcess.execute(new <String>["-s", id, "shell", "ip", "route"])
		}
		
		protected function onReadLanExit(ev:AdbProcessEvent):void {
			ev.currentTarget.removeEventListener(AdbProcessEvent.ADB_EXIT, onReadLanExit);
			if (ev.error != null) {
				trace(ev.error)
				return
			}
			
			var ipRouteDataUdp:Array = ev.stackedOutput;
			for(var i:int=0;i<ipRouteDataUdp.length;i++) {
				if(ipRouteDataUdp[i].indexOf("dev") > -1 && ipRouteDataUdp[i].indexOf("wlan0") > -1) {
					var ipData:Array = (ipRouteDataUdp[i] as String).match(/src ((192\..*)|(10\..*)|(172\..*))/);
					if(ipData != null && ipData.length >= 2){
						this.ip = StringUtil.trim(ipData[1] as String);
					}
				}
			}
			
			if(!ip) {
				writeErrorLogFile("WIFI not connected");
				error = "WIFI_NOT_CONNECTED"
				errorMessage = "Please connect the device to the local network"
				status = WIFI_ERROR;
			}else{
				uninstallDriver()
			}
		}
		
		override protected function executeDriver():void {
			var arguments: Vector.<String> = new <String>[
				"-s", id, "shell", "am", "instrument", "-w",
				"-e", "ipAddress", ip,
				"-e", "atsPort", port,
				"-e", "usbMode", String(usbMode),
				"-e", "serial", id,
				"-e", "debug", "false",
				"-e", "class", ANDROID_DRIVER + ".AtsRunnerWifiTest", ANDROID_DRIVER + ANDROID_JUNIT_RUNNER
			]
			startTestProcess(arguments);
		}
	}
}
