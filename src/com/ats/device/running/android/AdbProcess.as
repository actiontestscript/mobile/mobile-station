package com.ats.device.running.android {
import com.ats.helpers.Settings;

import flash.desktop.NativeProcess;
import flash.desktop.NativeProcessStartupInfo;
import flash.events.EventDispatcher;
import flash.events.NativeProcessExitEvent;
import flash.events.ProgressEvent;

public class AdbProcess extends EventDispatcher {

    public function AdbProcess(device:AndroidDevice) {
        this.device = device
    }
    private var output:String
    private var error:String;
    private var proc:NativeProcess;
    private var device:AndroidDevice

    public function terminate():Boolean {
        if (proc != null && proc.running) {
            proc.exit(true);
            return true;
        }
        return false
    }

    public function injectCommand(s:String):void {
        proc.standardInput.writeUTFBytes(s);
    }

    public function execute(arguments:Vector.<String>):void {
        output = "";
        error = "";

        var info:NativeProcessStartupInfo = new NativeProcessStartupInfo();
        info.executable = Settings.adbFile;
        info.arguments = arguments;

        proc = new NativeProcess();

        proc.addEventListener(NativeProcessExitEvent.EXIT, onExitHandler)
        proc.addEventListener(ProgressEvent.STANDARD_OUTPUT_DATA, onOutputHandler)
        proc.addEventListener(ProgressEvent.STANDARD_ERROR_DATA, onErrorHandler)

        proc.start(info)

        device.writeInfoLogFile(info.executable.nativePath + " " + arguments.join(" "))
    }

    private function onOutputHandler(event:ProgressEvent):void {
        var out:String = proc.standardOutput.readUTFBytes(proc.standardOutput.bytesAvailable)
        output += out
        dispatchEvent(new AdbProcessEvent(AdbProcessEvent.ADB_PROGRESS, null, out))

        device.writeInfoLogFile(out)
    }

    private function onErrorHandler(event:ProgressEvent):void {
        var err:String = proc.standardError.readUTFBytes(proc.standardError.bytesAvailable)
        error += err
        dispatchEvent(new AdbProcessEvent(AdbProcessEvent.ADB_ERROR, error))

        device.writeErrorLogFile(err)
    }

    private function onExitHandler(event:NativeProcessExitEvent):void {
        proc.removeEventListener(ProgressEvent.STANDARD_OUTPUT_DATA, onOutputHandler)
        proc.removeEventListener(ProgressEvent.STANDARD_ERROR_DATA, onErrorHandler)
        proc.removeEventListener(NativeProcessExitEvent.EXIT, onExitHandler)

        dispatchEvent(new AdbProcessEvent(AdbProcessEvent.ADB_EXIT, error, null, output));

        output = null
        error = null
        proc = null
        device = null
    }
}
}
