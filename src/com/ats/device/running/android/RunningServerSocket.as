package com.ats.device.running.android
{
	import flash.events.ServerSocketConnectEvent;
	import flash.net.ServerSocket;

	public class RunningServerSocket
	{
		public var used:Boolean = true;
		public var socket:ServerSocket;
		
		public function RunningServerSocket(port:int, connectFunc:Function)
		{
			socket = new ServerSocket();
			socket.bind(port);
			listener = connectFunc;
			socket.listen();
		}
		
		public function get localPort():int{
			return socket.localPort;
		}
		
		public function set listener(connectFunc:Function):void{
			used = true;
			socket.addEventListener(ServerSocketConnectEvent.CONNECT, connectFunc, false, 0, true);
		}
		
		public function removeListener(connectFunc:Function):void{
			used = false;
			socket.removeEventListener(ServerSocketConnectEvent.CONNECT, connectFunc);
		}
	}
}