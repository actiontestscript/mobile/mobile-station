//Licensed to the Apache Software Foundation (ASF) under one
//or more contributor license agreements.  See the NOTICE file
//distributed with this work for additional information
//    regarding copyright ownership.  The ASF licenses this file
//to you under the Apache License, Version 2.0 (the
//"License"); you may not use this file except in compliance
//with the License.  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//Unless required by applicable law or agreed to in writing,
//software distributed under the License is distributed on an
//"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
//KIND, either express or implied.  See the License for the
//specific language governing permissions and limitations
//under the License.

import Foundation
import XCTest
import Swifter

extension ButtonController: Routeable {
    
    var name: String { return "sysbutton" }
    
    func handleRoutes(_ request: HttpRequest) -> HttpResponse {
        guard let buttonName = String(bytes: request.body, encoding: .utf8) else {
            return .internalServerError
        }
        
        guard let action = ATSDevice.Button(rawValue: buttonName) else {
            return Output(message: "button \(buttonName) not exists", status: "-51").toHttpResponse()
        }
        
        switch action {
        case .lock:
            XCUIDevice.shared.perform(NSSelectorFromString("pressLockButton"))
            return Output(message: "press \(action.rawValue) button").toHttpResponse()
        default:
            return pressButton(action)
        }
    }
}

final class ButtonController {
    
    enum ButtonControllerError: Error {
        case unknowButton
    }
    
    private func pressButton(_ action:ATSDevice.Button) -> HttpResponse {
        if let deviceButton = transformAction(action) {
            XCUIDevice.shared.press(deviceButton)
            return Output(message: "press \(action.rawValue) button").toHttpResponse()
        } else {
            return Output(message: "button \(action.rawValue) not available on this device", status: "-60").toHttpResponse()
        }
    }
    
    private func transformAction(_ action:ATSDevice.Button) -> XCUIDevice.Button? {
        
#if targetEnvironment(simulator)
        switch action {
        case .home:
            return XCUIDevice.Button.home
        default:
            return nil
        }
#else
        switch action {
        case .home:
            return XCUIDevice.Button.home
        case .volumeDown:
            return XCUIDevice.Button.volumeDown
        case .volumeUp:
            return XCUIDevice.Button.volumeUp
        case .lock:
            return nil
        }
#endif
        
    }
}
