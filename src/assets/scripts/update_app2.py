import zipfile
import os
import sys
import subprocess

zipFilePath = sys.argv[1]
appParentFolderPath = sys.argv[2]
appFolderName = sys.argv[3]
appExeName = sys.argv[4]

appFolderPath = appParentFolderPath + "/" + appFolderName

# ----------------------------------------------------------------
# Kill GMSAAS
# ----------------------------------------------------------------

subprocess.call("taskkill /f /IM gmadbtunneld.exe")

# ----------------------------------------------------------------
# Unzip
# ----------------------------------------------------------------

with zipfile.ZipFile(zipFilePath, "r") as zip_ref:
    zip_ref.extractall(appFolderPath)

# ----------------------------------------------------------------
# Restart app
# ----------------------------------------------------------------

os.chdir(appFolderPath)
os.remove(zipFilePath)
os.system(appExeName)
