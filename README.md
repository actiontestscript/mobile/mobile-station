# AtsMobileStation
AtsMobileStation is an application that manages the installation and startup of mobile drivers used by ATS test framework.  
AtsMobileStation is available on Windows (Android testing) and macOS systems (Android and iOS testing).

## Installation on Windows

### Requirements
- C++ Redistributable Packages for Visual Studio 2013 32 bits (https://www.microsoft.com/fr-FR/download/details.aspx?id=40784)

## Installation on macOS

**Before unzipping and using AtsMobileStation on macOS, complete the following steps:**

1. Install openssl
- If Brew utility not already installed, open Terminal.app and type:  
  `/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)"`
- Then run command:  
  `brew install openssl@1.1`

2. Bypass Apple Gatekeeper
- Download AtsMobileStation zip file.
- Before unzipping file, open Terminal.app and type:  
  `xattr -r -d com.apple.quarantine /path/of/your/zip/file`
- Unzip the file and open it. If macOS popup asking for permission to open AtsMobileStation appears, delete the unzipped file and repeat the previous step.

## Android testing

### Requirements
- macOS Catalina or Windows
- AtsMobileStation

### Devices configuration
- Disable device pin code 
- Enable **developer mode** and authorize the connection to the linked computer

**For Mi or Xiaomi Device :**

- Go to Settings
- Additional Settings
- Developer options
- Enable settings : 
  - USB debugging
  - Install via USB
  - USB debugging (security settings)
  - Enable view attribute inspection
  - SSID Bridging Fast Handover & auto connection
  - And the most important setting : **Turn off MIUI optimization** !
    `(the MIUI optimization toggle can be hidden on your device, to make it appear, open developer option, under AUTO-FILL section, tap "reset to default values" few times, then the toggle will appears)`

## iOS testing 

### Requirements
- macOS Catalina 
- Xcode (download it on App Store)
- AtsMobileStation for macOS
- An Apple Developer account, and a developer license to test on physical devices 

### Setup provisionning profile into Xcode
- Open Xcode and go to 'Preferences' (Click on Xcode code in the left top corner)
- In 'Accounts', add a new account using '+' button in the bottom left corner
- Select 'Apple ID' in the dropdown list and enter your Apple Developer credentials in the next window
- When you're logged, click on "Manage Certificates". If there is no certificate associated with your workstation (no one is enlightened), click on "+" button -> "Apple Development" and "Done"

### AtsMobileStation configuration
- Open AtsMobileStation
- In the 'Connected devices' section, click on Apple logo
- In the textbox, put your Development team ID

### Devices configuration
- Disable device pin code 
- Enable Wi-Fi

### Error handling
- If the Apple keychain access ask credential recurrently: 
  - unplug the device
  - open the keychain access application
  - right click on "session"
  - "**lock** the keychain 'session'"
  - right click on "session"
  - "**unlock** the keychain 'session'"
  - enter your Mac credentials
  - plug your device again

## Mobile Station Server

### Continous integration - Install application from MobileStation
It is possible to install an application (Android/iOS) from MobileStation in order to integrate this step into a continuous integration process

curl -H "Content-Type: multipart/form-data" -F "file=@C:/app.apk" http://192.168.1.22:9000


ATSMobileServer centralizes all mobile endpoints accessible from a given PC/server.
​
ATSMobileServer can connect to your **physical endpoints**, with **mobile endpoints in the cloud** (via the Genymotion solution, which is also available on the desktop), but also perform tests on emulated Android endpoints installed on your machine.
​
There are several of them, and we do not have an exhaustive list of terminals supported by ATSMobileServer, in general, **an access to adb** is sufficient for us to support them.
​
Find a video with information about ATSMobileServer by watching this tutorial :[ ](https://youtu.be/i1AVPmaSNKQ)<https://youtu.be/i1AVPmaSNKQ>



## **Mobile Station User Interface**
​​
![image info](pictures/screen1.png)

The interface can be divided into 4 parts:
​
1. The top banner shows the MobileStation version numbers *("1.5.3" for this example)* and it also shows the network address and the port used (192.168.1.14:9000), which can be changed in the preferences.
1. The main screen, which shows the connected mobiles list *(there are no connected devices on the screenshot)*
1. The “Available simulators” banner, which offers iOS and Android simulators.
1. The preferences, composed of several useful information:

​
![image info](pictures/screen2.png)

 - Mobile Station Identifier: it is not usable but it is an information that can be asked by the Support team.
​
 - Python folder: A field to configure where Python is installed on the computer.
​
 - Genymotion Cloud: To be able to set up simulators in the cloud
​
 - Apple Development team ID: Important in the configuration to launch the tests on iOS phones
​
 - Automatic update: To activate automatic updates or not. If it’s not activated you have to do them manually with a button that will appear then. The “Custom Server” button is used to recover MobileStation updates to a server outside of Agilitest.
​
 - Web server: To change the port of your server
​
 - Useful links: A list of useful links

### **Open a simulator**

Mobile Station offers the possibility to use mobile simulators. A whole list of simulators will be proposed to you thanks to the installation of Xcode which is a prerequisite for the proper functioning of Mobile Station on macOS. For Android simulators on Windows and macOS, you will need to install Android Studio
​

*To open a simulator, click on the “**Available simulators**” banner and select a mobile simulator*

![image info](pictures/screen3.png)
​

​
Also, running iOS devices will only be done on a Mac, while running on Android can be done on both Windows and Mac.

​
When a simulator is activated a window will open with the server and it will appear in the list of the connected devices.

​
A loading icon is going to appear which means that the Driver is being installed on the simulator. Once the installation is done the icon will be replaced by a green check.
​

![image info](pictures/screen4.png)


For every device, there will be options. 
​
- You can click on the “Home” button to show the Home screen of the mobile. 
- You can also click on the configuration button to configure the mobile in which you can modify the port, automatically or manually, and you can “copy the device ID”.
​

Access to the “Home” and “Settings” buttons:

![image info](pictures/screen5.png)
​

*Click on the “**Settings**” to configure the mobile*

![image info](pictures/screen6.png)

​
If the mobile is an Android the USB mode can be activated

​
![image info](pictures/screen7.png)
​
### **Mobile tests execution**


What is important to know about MobileStation is that it is possible to launch the execution of the mobile with 2 different modes : **Wifi** and **USB**.
​

However, Iphone simulators or physical Iphone connected to Agilitest can only be launched with the Wifi mode.
​

The simulator is not closed directly on the main screen, you have to return to the "**Available simulators**" and then click again on the button of the concerned simulator.
​

*Click on the button to close the simulator from the list of available simulators*

![image info](pictures/screen8.png)
​
​

It is also possible to directly **connect a physical phone** to your computer and perform tests on it through Agilitest. When a phone is plugged in, it will first be recognized and put back in the list of connected mobiles with the same loading icon seen before, then the appearance of the green check.
​

For an Android phone, if you change the connection mode to USB, the phone will reset and then return to the list with an icon corresponding to the USB mode.

![image info](pictures/screen9.png)
